#!/usr/bin/python3
# (C) Hermann Lauer 2020
# can be distributed under the GPL V3 or later
"""NAME
        %(prog)s - <one line short description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

CREDITS
        Tommi2Day, https://github.com/datenschuft/SMA-EM.git

AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""

debug = 0
l=None

def run(emparts,config):
    import time

    supply=emparts['psupply']-emparts['pconsume']
#    print("{}W {psupplycounter:.4f}/{pconsumecounter:.4f}kWh {i1:.3f}|{i2:.3f}|{i3:.3f}A {u1:.3f}|{u2:.3f}|{u3:.3f}V {cosphi}".format(supply,**emparts))

    if l:
        l.send(b"%i 0"%(supply*10))
        return
    bits=0
    sleep=0
    if supply<0:
        bits|=4
        sleep=0.9-(supply/-500)
        bits2=bits
        if sleep>0: bits|=2
    elif supply>6800: bits|=1
    else: bits|=2
    g.write('D16',bits,7)
    if sleep>0:
        print(sleep)
        time.sleep(sleep)
        g.write('D16',bits2,7)

def stopping(emparts,config):
    pass

def on_publish(client,userdata,result):
    pass

def config(config):
    global debug
    debug=int(config.get('debug', 0))
    print('showled: feature enabled')

class LedUDP:
  def __init__(s,host='10.0.19.6',port=4331):
    import socket, syslog
    try:
      _,_,ips = socket.gethostbyaddr(host)
      s.host = ips[0]
      s.port = port
    except:
      syslog.syslog(syslog.LOG_ERR,"Could not resolve host: {}".format(host))

  def reset(s):
    """send a magic reset packet"""
    s.send(b"\x11")

  def resync(s): pass

  def send(s,data):
    import socket, syslog
    so = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    so.connect((s.host, s.port))
    so.send(data)
    so.close()

try:
    from bananaio2 import gpio
    g=gpio(needatomic=True)
    g.setDir('D16',1)
    g.setDir('D17',1)
    g.setDir('D18',1)
except (ModuleNotFoundError,PermissionError):
    l=LedUDP()
