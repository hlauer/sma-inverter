#!/usr/bin/python3
# (C) Hermann Lauer 2020
# can be distributed under the GPL V3 or later
"""NAME
        %(prog)s - <one line short description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

CREDITS
        Tommi2Day, https://github.com/datenschuft/SMA-EM.git

AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""
version="$Id: template.py 32668c4b1c2a 2019/08/20 14:53:55 Hermann $"

import time

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder,BinaryPayloadBuilder
from pymodbus.client.sync import ModbusTcpClient as ModbusClient

debug = 0

# Modbus decoding: #words, modbus function, NaN value
datatype = {
    'S16': (1,'decode_16bit_int', -0x8000),
    'S32': (2,'decode_32bit_int', -0x80000000),
    'U16': (1,'decode_16bit_uint',0xFFFF),
    'U32': (2,'decode_32bit_uint',0xFFFFFFFF),
    'U64': (4,'decode_64bit_uint',0xFFFFFFFFFFFFFFFF),
}

# scaling / TAGLIST decoding
def FIX0(i,reg): return i
def FIX1(i,reg): return i/10
TEMP=FIX1
def FIX2(i,reg): return i/100
def FIX3(i,reg): return i/1000
def TAGLIST(i,reg):
    try: return tags[reg][i]
    except KeyError: return i

tags = {
    'OpHealth': {
	35: 'Alm',
	303: 'Off',
	307: '',
	455: 'Wrn'
	},
    'DrtStt':{
	557: 'TmpDrt',
	884: '',
	1705:'HzDrt',
	3520:'VDrt',
	16777213: '-'
	},
    'GriSwStt':{51: '',
	311: 'nonFeed'
	},
    'OpStt':{295: 'Mpp',
	381: 'Stop',
	443: 'VolDCConst',
	455: 'Warn',
	1392: 'Fault',
	1393: 'WaitPV',
	1467: 'Start',
	1469: 'Shtdwn',
	1480: 'WaitUtil',
	2119: 'Derating',
	16777213: '-'
	}
}

class RS:
    """Holds a set of registers read together
"""
    def __init__(s,*desc):
        if type(desc[0])==type('') and len(desc[0])!=1:
#            print(desc)
            desc=(desc,)
        words=0
        flatdesc=[]
#        print(desc)
        for d in desc:
#            print(d)
            typ=d[0]
            cnt=1
            if typ[0].isdigit():                #an array of values
                cnt=int(typ[0])
                typ=typ[1:]
#            print(typ,cnt)
            words+=datatype[typ][0]*cnt        #calculate len to be read
            flatdesc.append((cnt,typ)+d[1:])    #flattened description
        s.len=words
        s.desc=flatdesc

    def __call__(s,bindat):
        data={}
        for cnt,typ,fix,name,unit in s.desc:
            val = [getattr(bindat,datatype[typ][1])() for i in range(cnt)]  #call conversion
            vals=[]
            for v in val:
                if v == datatype[typ][2]: vals.append(None)    #compare for NaN
                else: vals.append(fix(v,name))                 #scale or from TAGLIST

            if debug: print(name,vals)
            data[name] = vals
            if len(vals)==1: data[name]=vals[0]
        return data

#max. alle 10 sec, max 5 Register!
shortregisters = (
    (30201,RS('U32',TAGLIST,'OpHealth','')),
    (30217,RS(('U32', TAGLIST, 'GriSwStt', ''),
              ('U32', TAGLIST, 'DrtStt', ''))),
    (30513,RS('U64', FIX3, 'ActWh', 'kWh')),
    (30769,RS(('S32', FIX3, 'DCA A', 'A'),
              ('S32', FIX2, 'DCV A', 'V'),
              ('5S32', FIX0, 'Power', 'W'))),
    (30783,RS('S32', FIX2, 'AC voltage', 'V')),
    (30803,RS(('U32', FIX2, 'Hz', 'Hz'),
              ('4S32', FIX0, 'VAr', 'VAr'),
              ('S32', FIX0, 'VA', 'VA'))),
    (30953,RS('S32', TEMP, 'TmpCab', '\xb0C')),
    (30957,RS(('S32', FIX3, 'DCA B', 'A'),
              ('S32', FIX2, 'DCV B', 'V'),
              ('S32', FIX0, 'DC power B', 'W'))),
    (30975,RS(('S32', FIX2, 'DclVol', 'V'),
              ('S32', FIX3, 'AC current', 'A'))),
    (31405,RS(('3U32', FIX0, 'WCtl', ''),
              ('S32', FIX3, 'VArCtl', 'VAr'))),
    (40029,RS('U32', TAGLIST, 'OpStt','')),
    )

registers=shortregisters[2:]+(
    (30051,RS('5U32',TAGLIST,'NameTemplate','')),
    (30201,RS(('U32',TAGLIST,'OpHealth',''),
              ('3U32', FIX0, 'HealthStt', 'W'))),
    (30211,RS('5U32',TAGLIST,'OpStatus','')),
    (30225,RS('S32', FIX3, 'Ris', 'k\u03a9')),
    (30251,RS('U32',FIX0,'RstrLokStt','')),
    (30521,RS(('U64', FIX0, 'operation time', 's'),
              ('U64', FIX0, 'feed-in time', 's'),
              ('3U32', FIX0, 'TotWhOut', 'xWh') )),
    (30581,RS('2U32', FIX0, 'GridMsTotWh', 'Wh')),
    (30825,RS('U32', FIX0, 'Q-VArMod','')),
    (30829,RS(('S32', FIX1, 'Q-VArNom','%'),
              ('S32', FIX2, 'PF-PF',''),
              ('U32', TAGLIST, 'PF-PFExt',''),
              ('U32', TAGLIST, 'P-WMod',''),
              ('U32', FIX0, 'P-W','W'),
              ('U32', FIX0, 'P-WNorm','%'))),
    (30925,RS('3U32',FIX0,'ComSoc','')),
    (31405,RS(('3U32', FIX0, 'WCtl', ''),
              ('S32', FIX3, 'VArCtl', 'VAr'))),
    (33001,RS(('3U32', FIX0, 'StandbyStt', ''))),
#    (34113,RS('S32', TEMP, 'CabTmpVal','\xb0C')),
#    (34609,RS('2S32', FIX0, 'TmpVal','\xb0C')),
    (40005,RS('3U32',FIX0,'CtrlType','')),
    (40109,RS('U32',FIX0,'GridGuardCntry','')),
    (40200,RS('U32', TAGLIST, 'VArMod','')),
    (40204,RS(('S32', FIX1, 'VArNom','%'),
              ('S32', FIX2, 'PF',''),
              ('U32', TAGLIST, 'PFExt',''),
              ('7U32', FIX0, 'WMaxLim_Ena',''))),
#    (40210,RS('6U32', FIX0, 'WMaxLim_Ena','')),
    (40915,RS('U32', FIX0, 'WMax','W')),
    (41131,RS('2U32', FIX2, 'StrVol','V')),
#nw (41187,RS('U32', TAGLIST, 'CtlMsSrc','')),
    (41199,RS('3U32', FIX0,'WMaxNom','%')),
#    (41249,RS(('S32', FIX2, 'InWRte','%'),
#              ('S32', FIX0, 'WMaxIn','%'))),
    (41255,RS(('S16', FIX2, 'WMaxLimPct','%'),
              ('S16', FIX2, 'VArNomPrc','%'))),
    (41319,RS('3U32', TAGLIST, 'VArMod','')),
    )


#-0x7249,0x8db7,32:0x8db78000
#41256: 0x8000
#41257: 0x80000000
def get_data(host,port=502,modbusid=3,registers=registers):

    client = ModbusClient(host=host, port=port)
    try:
        client.connect()
    except Exception as e:
        print(e)
        return

    data = {}

    for address,reg in registers:
        try:
            recv = client.read_input_registers(address=address, count=reg.len, unit=modbusid)
        except Exception as e:
            print(e)
            return

        bindat = BinaryPayloadDecoder.fromRegisters(recv.registers, byteorder=Endian.Big, wordorder=Endian.Big)
        data.update(reg(bindat))

    client.close()
    return data

def write_data(host,address,val,port=502,modbusid=3):
    import struct

    client = ModbusClient(host=host, port=port)
    try:
        client.connect()
    except Exception as e:
        print(e)
        return

    global builder
    builder=BinaryPayloadBuilder(byteorder=Endian.Big,wordorder=Endian.Big)
    builder.add_16bit_int(70*100)
    payload = builder.build()

    global recv
    recv = client.write_register(address=address, value=val, unit=modbusid)

#    bindat = BinaryPayloadDecoder.fromRegisters(recv.registers, byteorder=Endian.Big, wordorder=Endian.Big)
    print(recv.value)

last_update = 0
led = None

def run(emparts,config):

    global last_update

    supply=emparts['psupply']-emparts['pconsume']
    print("{}W {psupplycounter:.4f}/{pconsumecounter:.4f}kWh {i1:.3f}|{i2:.3f}|{i3:.3f}A {u1:.3f}|{u2:.3f}|{u3:.3f}V {cosphi}".format(supply,**emparts))

    if time.time() < last_update + int(config.get('min_update', 20)):
        if (debug > 1):
            print("inverter: modbus query skipped")
        if led: led.send(b"%i"%(supply*10))
        if pv: pv.supply=int(supply*10)
        return

    last_update = time.time()

    data = get_data(config.get('host'),config.get('port', 502),int(config.get('modbus_id', 3)),shortregisters)
    if data is None:
        if debug > 0:
            print("inverter: no modbus data available" )

    if debug > 0:
            print("inverter:" + format(data))

    print("{DCV A}/{DCV B}>{DclVol}>{AC voltage}V  {Power[0]}/{DC power B}->({Power[2]}|{Power[3]}|{Power[4]}){Power[1]}W+{VAr[0]}VAr {AC current}A {Hz}Hz {ActWh}kWh {TmpCab}°C {OpHealth} {DrtStt} {OpStt}".format(**data))
    power=data['Power'][1]
    if led:
        if power is None: pkt=b"%i nan"%(supply*10)
        else:             pkt=b"%i %i"%(supply*10,power)
        led.send(pkt)
    if pv:
        pv.supply=int(supply*10)
        if power is None: pv.power=0
        else:             pv.power=int(power*10)
        temp=data['TmpCab']
        if temp is None: temp=float('nan')
        pv.temp=temp
        pv.schedtime=int(last_update)

def stopping(emparts,config):
    pass

def on_publish(client,userdata,result):
    pass

def config(config):
    global debug
    debug=int(config.get('debug', 0))
    print('inverter: feature enabled')

#send data via UDP to led
class LedUDP:
  def __init__(s,host='10.0.19.6',port=4331):
    import socket, syslog
    try:
      _,_,ips = socket.gethostbyaddr(host)
      s.host = ips[0]
      s.port = port
    except:
      syslog.syslog(syslog.LOG_ERR,"Could not resolve host: {}".format(host))

  def reset(s):
    """send a magic reset packet"""
    s.send(b"\x11")

  def resync(s): pass

  def send(s,data):
    import socket, syslog
    so = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    so.connect((s.host, s.port))
    so.send(data)
    so.close()

#-------------------------------------------------------------------------------
import argparse
class MyHelpFormatter(argparse.RawTextHelpFormatter):
    # It is necessary to use the new string formatting syntax, otherwise
    # the %(prog) expansion in the parent function is not going to work
    def _format_text(self, text):
        if '{version}' in text:
            text = text.format(version=version)
        return super(MyHelpFormatter, self)._format_text(text)

def ManOptionParser():
    return argparse.ArgumentParser(description=__doc__,
                                       formatter_class=MyHelpFormatter)

#-------------------------------------------------------------------------------
#    Main
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys

    parser = ManOptionParser()

    parser.add_argument('-d', dest='debug', type=int,
                        help='debug level (0-2)')

    parser.add_argument('host', nargs=1, help='hostname')

    args = parser.parse_args()
    debug=args.debug
    print(get_data(args.host[0]))

else:
    led=LedUDP()

    try:
        from pvmmap import pv
    except (ImportError,FileNotFoundError) as e:
        print(e)
        pv=None
