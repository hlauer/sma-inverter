# (C) Hermann Lauer 2020
# can be distributed under the GPL V3 or later

#structure to share PV data
from ctypes import Structure, c_int, c_uint, c_float

class PV(Structure):
    """values of PV inverter"""
    _fields_ = [("schedtime", c_uint),("supply",c_int),("power",c_uint),('status',c_uint),
                 ("temp",c_float)]

from memmap import StructMap
pv=StructMap(PV,"pv.mmap")
